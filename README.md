<p align="center"><img src="https://res.cloudinary.com/dtfbvvkyp/image/upload/v1566331377/laravel-logolockup-cmyk-red.svg" width="400"></p>

## Work Tracker

Application that allows to log work hours and track them.


### Demo

Demo available on [Heroku](http://pm-work-tracker.herokuapp.com/).
```
Email:    jan.kowalski@example.com
Password: password
```

### Application overview
Application is written in Laravel framework and Vue.js.\
Main application is a SPA written in Vue.js (entry point is on _/app_ URL) and it comunicates with Laravel RESTful API.\
Laravel Mix is used to bundle assets such as JS and CSS files into one file.\
__PostgreSQL__ must be used as database!

### Running application locally
#### Docker (recommended)
Recommended way is to use Docker and Docker Compose.
Application contains of 4 containers:
- php
- nginx
- postgres
- postgres_test

```shell
$ cp .env.docker .env

# run containers
$ docker-compose up

$ docker exec php php artisan key:generate

# create tables and run database seeders to seed with randomly generated data
$ docker exec php php artisan migrate --seed
```
Application should be running on port [8080](http://localhost:8080).

#### Using Artisan command
You have to create 2 PostgreSQL databases: **work_tracker** and **work_tracker_test**.\
Rememer to change environments in __phpunit.xml__ and __.env__.

```shell
$ cp .env.example .env

# Fill .env file with your environments!

$ php artisan key:generate

# run app
$ php artisan serve

# create tables and run database seeders to seed with randomly generated data
$ php artisan migrate --seed
```
Application should be running on port [8000](http://localhost:8000).

### Testing
There are a few feature tests written using PHPUnit.

```bash
# when using Docker
docker exec php vendor/bin/phpunit

# when using Artisan server
vendor/bin/phpunit
```
