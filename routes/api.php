<?php

Route::pattern('id', '[0-9]+');

Route::prefix('work-logs')->middleware(['auth:api'])->group(function () {
   Route::get('', 'Api\WorkLogController@getAll');
   Route::post('', 'Api\WorkLogController@createWorkLog');
   Route::patch('{id}', 'Api\WorkLogController@finishWorklog');

   Route::get('stats', 'Api\WorkLogController@getAllWorkLogsStats');
});
