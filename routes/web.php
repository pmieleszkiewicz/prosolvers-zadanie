<?php

Auth::routes();

Route::get('/', function () {
    return redirect()->route('login');
});

Route::any('app/{query?}', 'AppController@index')->where('query', '.*')
    ->middleware(['auth'])->name('app');
