<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\WorkLog;
use Carbon\Carbon;
use Faker\Generator as Faker;

$factory->define(WorkLog::class, function (Faker $faker) {
    $startedAt = $faker->dateTimeBetween('-1 months', 'now');

    return [
        'started_at' => $startedAt,
    ];
});
