<?php

use App\Models\User;
use App\Models\WorkLog;
use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;

class WorkLogSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // create 25 WorkLogs for each user where the last one is not finished
        User::all()->each(function(User $user) {
            factory(WorkLog::class, 24)->make()->each(function (WorkLog $workLog) use ($user) {
                $workLog->user()->associate($user);
                $workLog->finished_at = Carbon::instance($workLog->started_at)
                        ->addHours(random_int(1, 8))
                        ->addMinutes(random_int(1, 60));
                $workLog->save();
            });
            WorkLog::create([
                'started_at' => now()->subHours(random_int(1, 4))->subMinutes(random_int(1, 59)),
                'finished_at' => null,
                'user_id' => $user->id
            ]);
        });

    }
}
