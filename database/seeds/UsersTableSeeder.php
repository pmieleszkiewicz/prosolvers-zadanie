<?php

use App\Models\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    const USER_NUMBER = 20;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        # create test user to always be able to login
        $user = factory(User::class)->make([
            'first_name' => 'Jan',
            'last_name' => 'Kowalski',
            'email' => 'jan.kowalski@example.com',
        ]);
        $user->save();

        # create randomly generated users
        factory(User::class, static::USER_NUMBER - 1)->create();
    }
}
