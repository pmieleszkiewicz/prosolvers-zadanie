<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class AppControllerTest extends TestCase
{
    use RefreshDatabase;

    private $user;

    protected function setUp(): void
    {
        parent::setUp();

        $this->user = factory(User::class)->create();
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_visit_homepage()
    {
        $response = $this->get('/');

        $response->assertRedirect('/login');
    }

    public function test_visit_app_not_authenticated()
    {
        $response = $this->get('/app');

        $response->assertRedirect('/login');
    }

    public function test_visit_app_authenticated()
    {
        $response = $this
            ->actingAs($this->user)
            ->get('/app');

        $response->status(200);
    }
}
