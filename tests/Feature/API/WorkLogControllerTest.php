<?php

namespace Tests\Feature;

use App\Models\User;
use App\Models\WorkLog;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Carbon;
use Tests\TestCase;

class WorkLogControllerTest extends TestCase
{
    use RefreshDatabase;

    private $user;

    protected function setUp(): void
    {
        parent::setUp();

        $this->user = factory(User::class)->create();
    }

    protected function generateWorkLogData()
    {
        $this->workLogPrevYear = factory(WorkLog::class)->create([
            'started_at' => Carbon::create(2019, 12, 29, 8, 5, 0),
            'finished_at' =>  Carbon::create(2020, 12, 29, 14, 0, 0),
            'user_id' => $this->user->id
        ]);
        $this->workLogPrevMonth = factory(WorkLog::class)->create([
            'started_at' => Carbon::create(2020, 1, 10, 8, 15, 0),
            'finished_at' =>  Carbon::create(2020, 1, 10, 16, 15, 0),
            'user_id' => $this->user->id
        ]);
        $this->workLogWeekAgo = factory(WorkLog::class)->create([
            'started_at' => Carbon::create(2020, 2, 7, 8, 15, 0),
            'finished_at' =>  Carbon::create(2020, 2, 7, 16, 15, 0),
            'user_id' => $this->user->id
        ]);
        $this->workLogToday = factory(WorkLog::class)->create([
            'started_at' => Carbon::create(2020, 2, 14, 8, 15, 0),
            'finished_at' =>  Carbon::create(2020, 2, 14, 16, 15, 0),
            'user_id' => $this->user->id
        ]);
        $this->workLogWeekForward = factory(WorkLog::class)->create([
            'started_at' => Carbon::create(2020, 2, 21, 8, 0, 0),
            'finished_at' =>  Carbon::create(2020, 2, 21, 16, 0, 0),
            'user_id' => $this->user->id
        ]);
    }


    public function test_get_work_logs_when_not_authenticated()
    {
        $response = $this->json('GET', '/api/work-logs');

        $response->assertStatus(401);
    }

    public function test_get_work_logs_when_no_query_params()
    {
        $response = $this->actingAs($this->user, 'api')
            ->json('GET', '/api/work-logs')
            ->assertStatus(422)
            ->assertJsonValidationErrors([
                'started_at',
                'finished_at',
                'started_scope'
            ]);
    }

    public function test_get_work_logs_with_invalid_query_params()
    {
        $response = $this->actingAs($this->user, 'api')
            ->json('GET', '/api/work-logs', [
                'created_at' => 'invalid date',
                'finished_at' => '2020-01-01',
                'started_scope' => 'invalid'
            ])
            ->assertStatus(422)
            ->assertJsonValidationErrors([
                'started_at',
                'finished_at',
                'started_scope'
            ]);
    }

    public function test_get_today_work_logs()
    {
        $this->generateWorkLogData();

        $response = $this->actingAs($this->user, 'api')
            ->json('GET', '/api/work-logs', [
                'started_at' => '14-02-2020',
                'finished_at' => '14-02-2020',
                'started_scope' => 'day'
            ])
            ->assertStatus(200)
            ->assertJsonStructure([
                'data',
                'links',
                'meta'
            ]);

        $data = $response->decodeResponseJson();
        $this->assertCount(1, $data['data']);
    }

    public function test_get_work_logs_from_this_month()
    {
        $this->generateWorkLogData();

        $response = $this->actingAs($this->user, 'api')
            ->json('GET', '/api/work-logs', [
                'started_at' => '14-02-2020',
                'finished_at' => '14-02-2020',
                'started_scope' => 'month'
            ])
            ->assertStatus(200)
            ->assertJsonStructure([
                'data',
                'links',
                'meta'
            ]);

        $data = $response->decodeResponseJson();
        $this->assertCount(3, $data['data']);
    }

    public function test_get_work_logs_from_this_and_previous_month()
    {
        $this->generateWorkLogData();

        $response = $this->actingAs($this->user, 'api')
            ->json('GET', '/api/work-logs', [
                'started_at' => '01-01-2020',
                'finished_at' => '14-02-2020',
                'started_scope' => 'month'
            ])
            ->assertStatus(200)
            ->assertJsonStructure([
                'data',
                'links',
                'meta'
            ]);

        $data = $response->decodeResponseJson();
        $this->assertCount(4, $data['data']);
    }

    public function test_get_work_logs_from_this_year()
    {
        $this->generateWorkLogData();

        $response = $this->actingAs($this->user, 'api')
            ->json('GET', '/api/work-logs', [
                'started_at' => '01-01-2020',
                'finished_at' => '14-02-2020',
                'started_scope' => 'year'
            ])
            ->assertStatus(200)
            ->assertJsonStructure([
                'data',
                'links',
                'meta'
            ]);

        $data = $response->decodeResponseJson();
        $this->assertCount(4, $data['data']);
    }

    public function test_get_work_logs_from_this_and_prev_year()
    {
        $this->generateWorkLogData();

        $response = $this->actingAs($this->user, 'api')
            ->json('GET', '/api/work-logs', [
                'started_at' => '01-01-2019',
                'finished_at' => '14-02-2020',
                'started_scope' => 'year'
            ])
            ->assertStatus(200)
            ->assertJsonStructure([
                'data',
                'links',
                'meta'
            ]);

        $data = $response->decodeResponseJson();
        $this->assertCount(5, $data['data']);
    }

    public function test_get_stats_not_authenticated()
    {
        $response = $this->json('GET', '/api/work-logs/stats')
            ->assertStatus(401);
    }

    public function test_get_stats_from_today()
    {
        $this->generateWorkLogData();

        $response = $this->actingAs($this->user, 'api')
            ->json('GET', '/api/work-logs/stats', [
                'started_at' => '14-02-2020',
                'finished_at' => '14-02-2020',
                'started_scope' => 'day'
            ])
            ->assertStatus(200)
            ->assertJsonStructure([
                'data'
            ]);

        $data = $response->decodeResponseJson();
        $data = $data['data'];

        $this->assertEquals('14-02-2020', $data[0]['date']);
        $this->assertEquals(480, $data[0]['sum']);
    }
}
