<?php

declare(strict_types=1);

namespace App\Http\Controllers\Api;

use App\Http\Requests\GetWorkLogRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\WorkLog;
use App\Http\Resources\WorkLog as WorkLogResource;

class WorkLogController extends Controller
{
    public function __construct()
    {
//        $this->middleware('throttle:60');
    }

    public function getAll(GetWorkLogRequest $request)
    {
        $startedAt = $request->get('started_at');
        $finishedAt = $request->get('finished_at');
        $startedScope = $request->get('started_scope', 'day');
        $perPage = $request->get('per_page', 10);

        $workLogs = WorkLog::filterByParams($startedAt, $finishedAt, $startedScope)
            ->where('user_id', $request->user()->id)
            ->orderBy('started_at', 'desc');

        return WorkLogResource::collection($workLogs->paginate($perPage));
    }

    public function createWorkLog(Request $request)
    {
        $workLog = WorkLog::create([
            'user_id' => $request->user()->id,
            'started_at' => now()
        ]);

        return new WorkLogResource($workLog);
    }

    public function finishWorkLog(Request $request, int $id)
    {
         $workLog = WorkLog::where([
            ['id', '=', $id],
            ['user_id', '=', $request->user()->id]
        ])->firstOrFail();

        $workLog->finished_at = now();
        $workLog->save();

        return new WorkLogResource($workLog->fresh());
    }

    public function getAllWorkLogsStats(GetWorkLogRequest $request)
    {
        $startedAt = $request->get('started_at');
        $finishedAt = $request->get('finished_at');
        $scope = $request->get('started_scope', 'day');

        $query = WorkLog::filterByParams($startedAt, $finishedAt, $scope)
            ->where('user_id', $request->user()->id);

        $results = WorkLog::getWorkLogStats($query, $scope);

        return response()->json([
            'data' => $results
        ]);
    }
}
