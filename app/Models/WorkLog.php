<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;

class WorkLog extends Model
{
    protected $fillable = [
        'started_at',
        'finished_at',
        'user_id'
    ];

    // accessors and mutators
    public function getDurationAttribute(): ?int
    {
        if (null === $this->finished_at) {
            return null;
        }
        return Carbon::create($this->finished_at)->diffInMinutes($this->started_at);
    }

    public function setFinishedAtAttribute($value)
    {
        if (null === $value) {
            return;
        }

        if ($value->diffInMinutes($this->started_at) < 1) {
            throw ValidationException::withMessages(['finished_at' => 'Duration must be greater than 0 minutes']);
        }
        $this->attributes['finished_at'] = $value;
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    // Local scopes

    /**
     * Filters query by started_at, finished_at and scope ('day', 'month', 'year')
     *
     * @param $query
     * @param string $startedAt
     * @param string $finishedAt
     * @param string $scope
     * @return mixed
     */
    public function scopeFilterByParams($query, string $startedAt, string $finishedAt, string $scope)
    {
        $startedDate = Carbon::create($startedAt);
        $finishedDate = Carbon::create($finishedAt);

//        dd($startedDate, $finishedDate);

        if ($scope === 'day') {
            $query->whereDate('started_at', '>=', $startedDate)
                ->where(function ($query) use ($finishedDate) {
                    $query->whereDate('finished_at', '<=', $finishedDate)
                        ->orWhereNull('finished_at');
                });

        } elseif ($scope === 'month') {
            $startedDate = $startedDate->startOfMonth();
            $finishedDate = $finishedDate->lastOfMonth();

            $query = $query->whereDate('started_at', '>=', $startedDate)
                ->whereDate('finished_at', '<=', $finishedDate);
        } elseif ($scope === 'year') {
            $query = $query->whereYear('started_at', '>=', $startedDate->year)
                ->whereYear('finished_at', '<=', $finishedDate->year);
        }
        return $query;
    }

    /**
     * Generates stats based on scope ('day', 'month', 'year')
     *
     * @param \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder $query
     * @param string $scope
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder
     */
    public static function getWorkLogStats($query, string $scope)
    {
        if ($scope === 'day') {
            $results = $query
                ->addSelect(DB::raw("to_char(started_at, 'DD-MM-YYYY') as date, sum(EXTRACT(epoch FROM finished_at - started_at) / 60)::INTEGER as sum"))
                ->groupBy(DB::raw("to_char(started_at, 'DD-MM-YYYY')"));
        } elseif ($scope === 'month') {
            $results = $query
                ->addSelect(DB::raw("to_char(started_at, 'MM-YYYY') as date, sum(EXTRACT(epoch FROM finished_at - started_at) / 60)::INTEGER as sum"))
                ->groupBy(DB::raw("to_char(started_at, 'MM-YYYY')"));
        } elseif ($scope === 'year') {
            $results = $query
                ->addSelect(DB::raw("to_char(started_at, 'YYYY') as date, sum(EXTRACT(epoch FROM finished_at - started_at)::INTEGER / 60) as sum"))
                ->groupBy(DB::raw("to_char(started_at, 'YYYY')"));
        }

        $results->orderBy('date');

        return $results->get();
    }
}
