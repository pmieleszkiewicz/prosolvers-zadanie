<?php

return [
    'side_menu' => [
        'general' => [
            'label' => 'General',
            'homepage' => 'Dashboard',
            'stats' => 'Stats'
        ],
        'management' => [
            'label' => 'Management',
            'users' => 'Users',
            'work_logs' => 'Work logs'
        ]
    ],
];
