<?php

return [
    'side_menu' => [
        'general' => [
            'label' => 'Menu główne',
            'homepage' => 'Dashboard',
            'stats' => 'Statystyki'
        ],
        'management' => [
            'label' => 'Zarządzanie',
            'users' => 'Użytkownicy',
            'work_logs' => 'Godziny pracy'
        ]
    ],
];
