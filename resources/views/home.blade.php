@extends('_layouts.app')

@section('content')
    {{--Vue.js entry point--}}
    <App :user="{{ $user }}" />
@endsection
