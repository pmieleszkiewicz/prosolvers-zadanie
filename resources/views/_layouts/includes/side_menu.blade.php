<div class="col-md-3 mb-3">
    <div class="list-group nav-list">
        <span class="text-muted">{{ __('app.side_menu.general.label') }}</span>
        <a href="{{ route('home.index') }}"
           class="list-group-item list-group-item-action {{ Route::is('home.*') ? 'active' : '' }}">
            <i class="fas fa-tachometer-alt"></i> {{ __('app.side_menu.general.homepage') }}
        </a>
        <a href="{{ route('work_log.index') }}"
           class="list-group-item list-group-item-action {{ Route::is('work_log.*') ? 'active' : '' }} }}">
            <i class="fas fa-chart-line"></i> {{ __('app.side_menu.general.stats') }}
        </a>

        <!-- Admin area -->
        <div class="dropdown-divider"></div>
        <span class="text-muted">{{ __('app.side_menu.management.label') }}</span>
        <a href="#"
           class="list-group-item list-group-item-action">
            <i class="fas fa-users"></i> {{ __('app.side_menu.management.users') }}
        </a>
        <a href="#"
           class="list-group-item list-group-item-action">
            <i class="fas fa-briefcase"></i> {{ __('app.side_menu.management.work_logs') }}
        </a>
    </div>
</div>
