const prefix = '/api';

export default {
    WORK_LOGS: {
        GET: `${prefix}/work-logs`
    }
}
