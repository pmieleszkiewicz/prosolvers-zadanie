import API from './api_routes'

export default {
  state: {
      token: '',
      user: null,
  },
  getters: {
      user(state) {
          return state.user;
      },
  },
  mutations: {
      setUser(state, payload) {
          state.user = payload;
          state.token = payload.api_token;
      },
  },
  actions: {
      getWorkLogs(context, payload) {
          return new Promise((resolve, reject) => {
              axios({
                  method: 'GET',
                  url: API.WORK_LOGS.GET,
                  params: payload
              })
                  .then(function (response) {
                      if (response.status === 200) {
                          resolve(response.data);
                      }
                  })
                  .catch(function (error) {
                      reject(error);
                  });
          })
      },
      getWorkLogsStats(context, payload) {
          return new Promise((resolve, reject) => {
              axios({
                  method: 'GET',
                  url: `${API.WORK_LOGS.GET}/stats`,
                  params: payload
              })
                  .then(function (response) {
                      if (response.status === 200) {
                          resolve(response.data);
                      }
                  })
                  .catch(function (error) {
                      reject(error);
                  });
          })
      },
      finishWorkLog(context, payload) {
          return new Promise((resolve, reject) => {
              axios({
                  method: 'PATCH',
                  url: API.WORK_LOGS.GET + `/${payload.id}`,
                  params: {
                      started_at: payload.started_at,
                  }
              })
                  .then(function (response) {
                      if (response.status === 200) {
                          resolve(response.data);
                      }
                  })
                  .catch(function (error) {
                      reject(error);
                  });
          })
      },
      startWorkLog(context, payload) {
          return new Promise((resolve, reject) => {
              axios({
                  method: 'POST',
                  url: API.WORK_LOGS.GET,
              })
                  .then(function (response) {
                      if (response.status === 201) {
                          resolve(response.data);
                      }
                  })
                  .catch(function (error) {
                      reject(error);
                  });
          })
      },
  }
};
