import Router from 'vue-router'

import DashboardView from './componenets/views/DashboardView';
import WorkLogView from './componenets/views/WorkLogView';

export default new Router({
    mode: 'history',
    base: '/app/',
    routes: [
        {
            path: '/',
            name: 'dashboard_view',
            component: DashboardView
        },
        {
            path: '/work-logs',
            name: 'work_log_view',
            component: WorkLogView
        },
    ],
})
